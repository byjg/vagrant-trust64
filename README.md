# Vagrant Ubuntu Trust64 (14.04)

This installation provides

* PHP 5.5.9
* MySQL 5.6
* NGinx zero-conf multiple domains + PHP5-FPM
* Apache (optional)
* Shared Folder: ./code
* Host IP: 10.10.100.100
* MySQL Username: root and password: vagrant

## installation

### First time

* Install the latest Virtualbox
* Install the latest Vagrant
* Install the vagrant-vbguest (see below)

```
vagrant plugin install vagrant-vbguest
```

### Running

```
vagrant up 
```

## Note about zero-conf multiple domains

This install enables you to run multiple domain with zero-conf in the Nginx.

### How it works?

Just create a symbolic link with the domain name inside the `<shared folder>`. Like this:

```
ln -s /path/to/my/project ./code/domain.name.com 
sudo sh -c 'echo "10.10.100.100    domain.name.com      # For Vagrant" >> /etc/hosts'
```

All inside that folder will be available to the nginx. 

If inside the folder `./code/domain.name.com` has the folder `public`, `web` or `httpdocs` the Nginx will use it as root folder. 

