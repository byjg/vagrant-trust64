#!/usr/bin/env bash

sudo aptitude remove -q -y -f nginx php5-fpm

sudo apt-get install -q -y -f libapache2-mod-php5
sudo a2enmod rewrite

sudo rm /etc/apache2/conf-available/httpd.conf
sudo touch /etc/apache2/conf-available/httpd.conf
sudo sh -c "cat >> /etc/apache2/conf-available/httpd.conf <<'EOF'
<Directory /vagrant/>
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
</Directory>
EOF"
sudo a2enconf httpd


sudo rm /etc/apache2/sites-enabled/000-default.conf
sudo touch /etc/apache2/sites-enabled/000-default.conf

sudo sh -c "cat >> /etc/apache2/sites-enabled/000-default.conf <<'EOF'
<VirtualHost *:80>
        ServerName localhost

        ServerAdmin webmaster@localhost
        DocumentRoot /vagrant

        ErrorLog \${APACHE_LOG_DIR}/error.log
        CustomLog \${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF"

sudo service apache2 restart
