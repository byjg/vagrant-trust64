#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive

sudo aptitude update -q

# Fix Locale
sudo aptitude install -q -f -y language-pack-pt
sudo locale-gen en_US.UTF-8 pt_BR.UTF8
sudo dpkg-reconfigure locales
sudo touch /etc/profile.d/10-environment.sh
sudo sh -c "cat >> /etc/profile.d/10-environment.sh << 'EOF'
export LC_ALL=pt_BR.UTF-8
export LANG=pt_BR.UTF-8
EOF"
sudo chmod a+x /etc/profile.d/10-environment.sh
sudo /etc/profile.d/10-environment.sh

# Force a blank root password for mysql
echo "mysql-server mysql-server/root_password password vagrant" | sudo debconf-set-selections
echo "mysql-server mysql-server/root_password_again password vagrant" | sudo debconf-set-selections

# Install mysql, nginx, php5-fpm
sudo aptitude install -q -y -f mysql-server mysql-client nginx php5-fpm vim-nox

# Install commonly used php packages
sudo aptitude install -q -y -f php5-mysql php5-curl php5-gd php-pear php5-imagick php5-imap php5-mcrypt php5-memcached php5-sqlite php5-tidy php5-xsl php5-xcache


sudo rm /usr/share/nginx/html/404.html
sudo touch /usr/share/nginx/html/404.html
sudo sh -c "sudo cat >> /usr/share/nginx/html/404.html <<'EOF'
<!DOCTYPE html>
<html>
<head>
<title>Not found</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Page not found.</h1>
<p>Sorry, the page you are looking for was not found.</p>
</body>
</html>
EOF"


sudo rm /etc/nginx/sites-available/default
sudo touch /etc/nginx/sites-available/default
sudo sh -c "sudo cat >> /etc/nginx/sites-available/default <<'EOF'
server {
  listen   80;

  index index.php index.html index.htm;

  # Make site accessible from http://localhost/
  server_name _;

  set \$domain \$host;

  # Remove 'www'
  if (\$domain ~ \"^(w{3}\.)(.*)\") {
    set \$domain \$2;
  }
  
  set \$base /vagrant/\$domain;

  # If does not exists, uses 127.0.0.1
  if ( !-d \$base ) {
    set \$domain "127.0.0.1";
    set \$base /vagrant/\$domain;
  }

  # Check for common public folders.
  if ( -d \$base/web ) {
    set \$base \$domain/web;
  }
  if ( -d \$base/httpdocs ) {
    set \$base \$domain/httpdocs;
  }
  if ( -d \$base/public ) {
    set \$base \$domain/public;
  }
  
  
  access_log /var/log/nginx/\$domain.access.log;
  error_log /var/log/nginx/error.log;

  root \$base/;

  location = /404.html {
    root /usr/share/nginx/html;
  }
	
  location / {
    # First attempt to serve request as file, then
    # as directory, then fall back to index.html
    try_files \$uri \$uri/ /route.php /index.php /404.html;
  }

  # pass the PHP scripts to FastCGI server listening on /tmp/php5-fpm.sock
  #
  location ~ \\.php$ {
    try_files \$uri =404;
    fastcgi_split_path_info ^(.+\.php)(/.+)$;
    fastcgi_pass unix:/var/run/php5-fpm.sock;
    fastcgi_index index.php;
    include fastcgi_params;
  }

  # deny access to .htaccess files, if Apache's document root
  # concurs with nginx's one
  #
  location ~ /\\.ht {
    deny all;
  }
}
EOF"

sudo mkdir -p /vagrant/127.0.0.1
sudo rm /vagrant/127.0.0.1/index.html
sudo touch /vagrant/127.0.0.1/index.html
sudo sh -c "cat >> /vagrant/127.0.0.1/index.html <<'EOF'
<h1>Your domain was not setup</h1>

Please read the README.md
EOF"

sudo mkdir -p /vagrant/10.10.100.100
sudo rm /vagrant/10.10.100.100/info.php
sudo touch /vagrant/10.10.100.100/info.php
sudo sh -c "cat >> /vagrant/10.10.100.100/info.php <<'EOF'
<?php phpinfo(); ?>
EOF"


sudo service nginx restart

sudo service php5-fpm restart

